import numpy as np
from rdkit import Chem
from tenn.utils.operations import create_compounds
from tenn.utils.operations import create_adjacency
from tenn.utils.operations import create_bonds
from tenn.utils.operations import create_atoms
from tenn.utils.operations import split_sequence
from collections import defaultdict
import os


dataset, k, ngram = sys.argv[1:]
k, ngram = map(int, [k, ngram])

with open('../../data/' + dataset + '/input.in', 'r') as f:
    molecules_list = f.read().strip().split('\n')

molecules_list = [d for d in data_list if '.' not in d.strip().split()[0]]
molecules_no = len(molecules_list)

atom_dict = defaultdict(lambda: len(atom_dict))
bond_dict = defaultdict(lambda: len(bond_dict))
compound_dict = defaultdict(lambda: len(compound_dict))
edge_dict = defaultdict(lambda: len(edge_dict))
seq_dict = defaultdict(lambda: len(seq_dict))

smiles_list, compounds_list, connections_list, proteins_list, rel_list = '', [], [], [], []
for idx, data in enumerate(molecules_list):
    print('/'.join(map(str, [idx + 1, molecules_no])))
    smiles_list, sequence, rel = data.strip().split()
    pom_smiles += smiles_list + '\n'
    molecule = Chem.AddHs(Chem.MolFromSmiles(smiles_list))
    atoms = create_atoms(molecule)
    xybond_dict = create_bonds(molecule)
    compounds_vec = create_compounds(atoms, xybond_dict, k)
    compounds_list.append(compounds_vec)
    adjacency = create_adjacency(molecule)
    connections_list.append(adjacency)
    words = split_sequence(sequence, ngram)
    proteins_list.append(words)
    rel_list.append(np.array([float(rel)]))

directory_save = ('../../data/' + dataset + '/input/')
os.makedirs(directory_save, exist_ok = True)
with open(directory_save + 'smiles.txt', 'w') as f:
    f.write(pom_smiles)
np.save(directory_save + 'compounds', compounds_list)
np.save(directory_save + 'connections', connections_list)
np.save(directory_save + 'proteins', proteins_list)
np.save(directory_save + 'drug_target', rel_list)
dump_dictionary(compound_dict, directory_save + 'compound_dict.pickle')
