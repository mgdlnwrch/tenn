import torch
from tenn.utils.operations import split_dataset
from tenn.utils.operations import shuffle_dataset
from tenn.tennmodel.TENN import TENN
from tenn.tennmodel.tester import Tester
from tenn.tennmodel.trainer import Trainer
from gensim.models import Word2Vec


if __name__ == "__main__":
    maxlength = 200
    size = 20
    sequence_len = maxlength * size
    dict_seq = get_sequence_dict()
    model_wv = Word2Vec.load('../../model/word2vec/word2vec_proteins.model'
    
    torch.cuda.set_device(0)
    (dataset, radius, ngram, dim, m_size, m_maxlength, layer_gnn, window, layer_cnn, layer_output,
     lr, lr_decay, decay_interval, weight_decay, iteration,
     setting) = sys.argv[1:]
    (dim, m_size, m_maxlength, layer_gnn, window, layer_cnn, layer_output, decay_interval,
     iteration) = map(int, [dim, m_size, m_maxlength, layer_gnn, window, layer_cnn, layer_output,
                            decay_interval, iteration])
    lr, lr_decay, weight_decay = map(float, [lr, lr_decay, weight_decay])
    if torch.cuda.is_available():
        device = torch.device('cuda')
    else:
        device = torch.device('cpu')
    entry_directory = ('../../data/' + dataset + '/input/')
    with open('../../data/' + dataset + '/prot.txt', encoding = 'utf8') as f:
        protein = f.read().strip().split('\n') 
    pro_embedding_matrix = get_protein_embedding(dict_seq, model_wv, protein, 2000, 100)
    
    with open('../../data/' + dataset + '/smile_n_gram.txt', 'r') as f:
        smile = f.read().strip().split('\n')
    tmp_smile, smi_word_index, smi_embedding_matrix = get_smiles_embedding(smile, 100, 100)
    np.save(entry_directory + "smile.npy", tmp_smile)
    del tmp_smile

    smiles = load_tensor(entry_directory + dataset + '/' + 'smile', torch.LongTensor)
    compounds = load_tensor(entry_directory + dataset + '/' + 'compounds', torch.LongTensor)
    connections = load_tensor(entry_directory + dataset + '/' + 'connections', torch.LongTensor)
    proteins = load_tensor(entry_directory + dataset + '/' + 'protein', torch.LongTensor)
    drug_target = load_tensor(entry_directory + dataset + '/' + 'drug_target', torch.FloatTensor)
    Y = load_tensor(entry_directory + dataset + '/' + 'drug_target', torch.FloatTensor)
    compound_dict = load_pickle(entry_directory + dataset + '/' + 'compound_dict.pickle')
    compound_length = len(compound_dict)

    Y = Y.argmax(1).cpu().numpy()
    Y = np.asarray(Y)
    drug_target = -(np.log10(Y / (math.pow(10, 9))))
    drug_target = list(drug_target)

    dataset = list(zip(compounds, connections, proteins, smiles, drug_target))
    dataset = shuffle_dataset(dataset, 1234)
    dataset_train, dataset_ = split_dataset(dataset, 2/3)
    dataset_dev, dataset_test = split_dataset(dataset_, 0.4)
    model = TENN(pro_embedding_matrix, smi_embedding_matrix, compound_length, dim).to(device)
    trainer = Trainer(model)
    tester = Tester(model)

    info = ('Epoch no.\tLoss_train\tMSE_dev\tMSE_test\trm2\t')

    #training
    for epoch in range(1, iteration):
        if epoch % decay_interval == 0:
            trainer.optimizer.param_groups[0]['lr'] *= lr_decay
        loss_train = trainer.train(dataset_train)
        MSE_dev = tester.test(dataset_dev)[0]
        MSE_test, rm2, corr_l, pred_v = tester.test(dataset_test)
        tmp_info = [epoch, loss_train, MSE_dev, MSE_test, rm2]
        print('\t'.join(map(str, tmp_info)))

if __name__ == "__main__":
    main()
