# About TENN: Triplet Encoded Neural Network

The architecture proposed in this work aims to identify the drug-target interactions.

## Requirements

You'll need to install following in order to run the codes.

*  [Python 3.7](https://www.python.org/downloads/)
*  [PyTorch]
*  numpy
