from sklearn.metrics import mean_squared_error
from tenn.utils.operations import calculate_rsquared_error


class Tester(object):
    def __init__(self, model):
        self.model = model

    def test(self, dataset):
        corr_l, pred_v = [], []
        for data in dataset:
            (correct_labels, predicted_values) = self.model(data, train = False)
            corr_l.append(correct_labels)
            pred_v.append(predicted_values)
        mse = mean_squared_error(T, S)
        rsqe2 = calculate_rsquared_error(corr_l, pred_v)
        return mse, rsqe2, corr_l, pred_v
