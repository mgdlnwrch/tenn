import torch
import torch.nn as nn
import torch.nn.functional as F
from torch_geometric.nn import global_max_pool as gmp


class TENN(nn.Module):
    def __init__(self, protein_embedding_matrix, smi_embedding_matrix, dim, compound_length):
        super(TENN, self).__init__()
        self.compound_representation = nn.Embedding(compound_length, dim)
        self.target_representation = nn.Embedding(100000, 100)
        self.target_representation.weight = nn.Parameter(torch.tensor(protein_embedding_matrix, dtype = torch.float32))
        self.target_representation.weight.requires_grad = True
        self.smile_representation = nn.Embedding(1000, 100)
        self.smile_representation.weight = nn.Parameter(torch.tensor(smi_embedding_matrix, dtype = torch.float32))
        self.smile_representation.weight.requires_grad = True
        self.gcn1 = GATConv(dim, dim, heads = 10, dropout = 0.2)
        self.gcn2 = GATConv(dim * 10, 128, dropout = 0.2)
        self.fc_g1 = nn.Linear(128, 128)
        self.W_cnn = nn.ModuleList([nn.Conv2d(
                     in_channels = 1, out_channels = 1, kernel_size = 2*window+1,
                     stride = 1, padding = window) for _ in range(layer_cnn)])
        self.W_attention = nn.Linear(dim, 100)
        self.P_attention = nn.Linear(100, 100)
        self.W_out = nn.ModuleList([nn.Linear(100+200+128, 100+200+128)
                                    for _ in range(layer_output)])
        self.prediction = nn.Linear(100+200+128, 1)
        self.lnn1 = nn.Linear(428, 512)
        self.lnn2 = nn.Linear(512, 428)
        self.fc1 = nn.Linear(428, 428)
        self.fc2 = nn.Linear(428, 428)
        self.relu = nn.ReLU()
        self.dropout = nn.Dropout(0.25)
        
        
    def lnn(self, xs):
        xs = F.selu(self.lnn1(xs))
        xs = self.lnn2(xs)
        return xs


    def cnn_processing(self, xs, layer):
        xs = torch.unsqueeze(torch.unsqueeze(xs, 0), 0)
        for i in range(layer_cnn):
            xs = self.W_cnn[i](xs)
            xs = torch.relu(xs)
        xs = torch.squeeze(torch.squeeze(xs, 0), 0)
        return torch.unsqueeze(torch.mean(xs, 0), 0)
    
        
    def forward(self, inputs):
        compounds, connection, proteins, smiles = inputs
        compound_vectors = self.self.compound_representation(compounds)
        connection = connection.transpose(1, 0)
        out1 = F.dropout(compound_vectors, p = 0.2, training = self.training)
        out1 = F.relu(self.gcn1(out1, connection))
        out1 = F.dropout(out1, p=0.2, training = self.training)
        out1 = self.gcn2(out1, connection)
        out1 = self.relu(out1)
        batch = torch.LongTensor([0]*len(out1)).to(device)
        out1 = gmp(out1, batch)
        out1 = self.fc_g1(out1)
        compound_vector = self.relu(out1)

        target_vectors = self.target_representation(proteins)
        protein_vector = self.lnn(target_vectors)
        
        smiles_vectors = self.smile_representation(smiles)
        out_smiles_vector = self.cnn_processing(smiles_vectors, layer_cnn)

        concatenated_vectors = torch.cat((compound_vector, protein_vector, out_smiles_vector), 1)
        final_output = self.fc1(concatenated_vectors)
        final_output = self.relu(final_output)
        final_output = self.dropout(final_output)
        final_output = self.fc2(final_output)
        final_output = self.relu(final_output)
        final_output = self.dropout(final_output)
        return self.prediction(final_output)


    def __call__(self, data, train = True):
        inputs, correct_prediction = data[:-1], data[-1]
        predicted_interaction = self.forward(inputs)
        predicted_interaction = torch.squeeze(predicted_interaction, 0)
        if train:
            loss_fuc = torch.nn.MSELoss()
            loss = loss_fuc(predicted_interaction, correct_prediction)
            return loss
        else:
            correct_labels = correct_prediction.to('cpu').data.numpy()
            ys = predicted_interaction.to('cpu').data.numpy()
            predicted_scores = ys
            return correct_labels, predicted_scores
