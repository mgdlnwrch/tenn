import numpy as np
from rdkit import Chem
from collections import defaultdict
import networkx as nx
import pickle
import copy
from keras.preprocessing import text, sequence


def load_tensor(file_name, dtype):
    return [dtype(d).to(device) for d in np.load(file_name + '.npy', allow_pickle = True)]


def load_pickle(file_name):
    with open(file_name, 'rb') as f:
        return pickle.load(f)


def split_dataset(input_data, ratio):
    n = int(ratio * len(input_data))
    input_data_1, input_data_2 = input_data[:n], input_data[n:]
    return input_data_1, input_data_2
    

def shuffle_dataset(input_data, seed):
    np.random.seed(seed)
    np.random.shuffle(input_data)
    return input_data
    

def token(dataset):
    token_dataset = []
    for i in range(len(dataset)):
        seq = []
        for j in range(len(dataset[i])):
            seq.append(dataset[i][j])
        token_dataset.append(seq)  
    return token_dataset
    
    
def pad(protein, max_length):           
    padded_protein = copy.deepcopy(protein)   
    for i in range(len(padded_protein)):
        if len(padded_protein[i])<max_length:
            for j in range(len(padded_protein[i]), max_length):
                padded_protein[i].append('J')
    return padded_protein
    

def get_residue_representation(vec, tokened_seq_protein, max_length, size):  
    represented_protein  = []
    for i in range(len(tokened_seq_protein)):
        temp_sentence = []
        for j in range(max_length):
            if tokened_seq_protein[i][j]=='J':
                temp_sentence.extend(np.zeros(size))
            else:
                temp_sentence.extend(vec[tokened_seq_protein[i][j]])
        represented_protein.append(np.array(temp_sentence))    
    return np.array(represented_protein)
    
    
def get_protein_representation(vec, protein_1, protein_2, max_length, size):
    token_protein_1 = token(protein_1)
    token_protein_2 = token(protein_2)
    padded_token_protein_1 = pad(token_protein_1, max_length)
    padded_token_protein_2 = pad(token_protein_2, max_length)
                   
    protein_1_repr  = get_residue_representation(vec, padded_token_protein_1, max_length, size)
    protein_2_repr  = get_residue_representation(vec, padded_token_protein_2, max_length, size)
    
    protein_12_repr = np.hstack((np.array(protein_1_repr), np.array(protein_2_repr)))

    return protein_12_repr


def get_protein_embedding(dict_seq, vec, idx_pairs, max_length, size):
    sequences = []
    for i in range(len(idx_pairs)):
        sequences.append([dict_seq[idx_pairs[i][0]], dict_seq[idx_pairs[i][1]]])
    sequences = np.array(sequences)
    token_seq_protein_1 = token(sequences[:, 0])
    token_seq_protein_2 = token(sequences[:, 1])
    
    tokened_token_seq_protein_1 = pad(token_seq_protein_1, max_length)
    tokened_token_seq_protein_2 = pad(token_seq_protein_2, max_length)

    feature_protein_1 = get_protein_representation(vec, tokened_token_seq_protein_1, max_length, size)
    feature_protein_2 = get_protein_representation(vec, tokened_token_seq_protein_2, max_length, size)
    
    feature_protein_12 = np.hstack((np.array(feature_protein_1), np.array(feature_protein_2)))     
    return np.array(feature_protein_12)


def get_smiles_embedding(smile, max_length, vector_size):
    tokenizer = text.Tokenizer(num_words = 1000, lower = False, filters = "　")
    tokenizer.fit_on_texts(smile)
    smile_ = sequence.pad_sequences(tokenizer.texts_to_sequences(smile), maxlen = max_length)
    word_index = tokenizer.word_index
    words_no = len(word_index)
    tmp_smile_vec = {}
    with open("../../data/embed/smiles_ngrams.csv", encoding = 'utf8') as f:
        for line in f:
            values = line.rstrip().rsplit(' ')
            word = values[0]
            coefs = np.asarray(values[1:], dtype = 'float32')
            tmp_smile_vec[word] = coefs
    count = 0
    embedding_matrix = np.zeros((words_no + 1, vector_size))
    for word, i in word_index.items():
        embedding = tmp_smile_vec[word] if word in tmp_smile_vec else None
        if embedding is not None:
            count += 1
            embedding_matrix[i] = embedding
        else:
            unk_vec = np.random.random(vector_size) * 0.5
            unk_vec = unk_vec - unk_vec.mean()
            embedding_matrix[i] = unk_vec
    del tmp_smile_vec
    return smile_, word_index, embedding_matrix
    
    
def get_sequence_dict():
    file_name = '../../data/protein'
    seq = []
    index = []
    with open(file_name, 'r') as file_:
        i = 0
        for line in file_:
            if i%2 == 0:
                index.append(int(line.strip().split(':')[1].split('P|')[0]))
            if i%2 == 1:
                seq.append(line.split('\n')[0])
            i = i+1    
    dict_seq = dict()
    for i in range(len(index)):
        dict_seq[index[i]] = seq[i]
    return dict_seq


def create_atoms(molecule, atom_dict):
    atoms = [a.GetSymbol() for a in molecule.GetAtoms()]
    for at in molecule.GetAromaticAtoms():
        i = at.GetIdx()
        atoms[i] = (atoms[i], 'aromatic')
    atoms = [atom_dict[at] for at in atoms]
    return np.array(atoms)


def create_bonds(molecule, bond_dict):
    xybond_dict = defaultdict(lambda: [])
    for mol_bond in molecule.GetBonds():
        x, y = mol_bond.GetBeginAtomIdx(), mol_bond.GetEndAtomIdx()
        bond = bond_dict[str(mol_bond.GetBondType())]
        x_ybond_dict[x].append((y, bond))
        x_ybond_dict[y].append((x, bond))
    return xybond_dict


def create_compounds(atoms, xybond_dict, paths_no):
    if (len(atoms) == 1) or (paths_no == 0):
        fingerprints = [compound_dict[at] for at in atoms]
    else:
        nodes = atoms
        xyedge_dict = xybond_dict
        for _ in range(paths_no):
            fingerprints = []
            for x, y_edge in xyedge_dict.items():
                neighbors = [(nodes[j], edge) for j, edge in y_edge]
                fingerprint = (nodes[x], tuple(sorted(neighbors)))
                fingerprints.append(compound_dict[fingerprint])
            nodes = fingerprints
            tmp_xy_edge_dict = defaultdict(lambda: [])
            for x, y_edge in xyedge_dict.items():
                for j, edge in y_edge:
                    both_side = tuple(sorted((nodes[x], nodes[j])))
                    edge = edge_dict[(both_side, edge)]
                    tmp_xy_edge_dict[i].append((j, edge))
            xyedge_dict = tmp_xy_edge_dict
    return np.array(fingerprints)


def create_adjacency(molecule):
    adjacency = Chem.GetAdjacencyMatrix(molecule)
    return np.array(adjacency)


def create_edge_index(molecule):
    edges_list = []
    for bond in molecule.GetBonds():
        edges_list.append([bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()])
    graph = nx.Graph(edges_list).to_directed()
    edge_idx = []
    for ei_1, ei_2 in graph.edges_list:
        edge_idx.append([ei_1, ei_2])
    return edge_idx


def split_sequence(sequence, ngram, seq_dict):
    sequence = '-' + sequence + '='
    seqs = [seq_dict[sequence[i:i + ngram]]
             for i in range(len(sequence)- ngram + 1)]
    return np.array(seqs)


def dump_dictionary(dictionary, filename):
    with open(filename, 'wb') as f:
        pickle.dump(dict(dictionary), f)
        
        
def calculate_k(y_obs, y_pred):
    y_obs = np.array(y_obs)
    y_pred = np.array(y_pred)
    return sum(y_obs * y_pred) / float(sum(y_pred * y_pred))


def calculate_rsquared_error(xs_orig, xs_line):
    xs_orig = np.concatenate(xs_orig)
    xs_line = np.concatenate(xs_line)
    r02 = squared_error0(xs_orig, xs_line)
    r2 = r_squared_error(xs_orig, xs_line)
    return r2 * (1 - np.sqrt(np.absolute((r2 * r2) - (r02 * r02))))
    
    
 def r_squared_error(y_obs, y_pred):
    y_obs = np.array(y_obs)
    y_pred = np.array(y_pred)
    y_obs_mean = [np.mean(y_obs) for y in y_obs]
    y_pred_mean = [np.mean(y_pred) for y in y_pred]
    mult = sum((y_pred - y_pred_mean) * (y_obs - y_obs_mean))
    mult = mult * mult
    y_obs_sq = sum((y_obs - y_obs_mean) * (y_obs - y_obs_mean))
    y_pred_sq = sum((y_pred - y_pred_mean) * (y_pred - y_pred_mean))
    return mult / float(y_obs_sq * y_pred_sq)
    
    
def squared_error0(y_obs, y_pred):
    k = calculate_k(y_obs, y_pred)
    y_obs = np.array(y_obs)
    y_pred = np.array(y_pred)
    y_obs_mean = [np.mean(y_obs) for y in y_obs]
    upp = sum((y_obs - (k * y_pred)) * (y_obs - (k * y_pred)))
    down = sum((y_obs - y_obs_mean) * (y_obs - y_obs_mean))
    return 1 - (upp / float(down))
